<?php

use Codeception\Util\HttpCode;

class UpdateEmployeeCest
{
    public function _before(ApiTester $I)
    {
        $I->sendPOST('/auth/login', ['email' => $I->email, 'password' => $I->password]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseJsonMatchesJsonPath('$.access_token');
        $I->token = $I->grabDataFromResponseByJsonPath('$.access_token')[0];
    }

    // tests
    public function updateEmployee(ApiTester $I)
    {
        $I->wantTo('Upload a new CSV File in order to update and create Employees');
        $I->amBearerAuthenticated($I->token);
        $I->sendPOST('/importar-colaborador/updateemployees');
        $I->seeResponseCodeIs(HttpCode::CREATED);
    }

    public function checkEmployees(ApiTester $I)
    {
        $I->wantTo('Check if csv really updated employees');
        $I->amBearerAuthenticated($I->token);
        $I->sendGET('/obter-colaboradores');
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'name' => 'Francisco das Neves',
            'email' => 'francisco.dasneves@desouza.br',
            'document' => '172.100.560-92',
            'city' => 'Santa Giovana',
            'state' => 'Rio de Janeiro',
            'start_date' => '2020-02-20',
            'manager_id' => '41b6c1f7-1d2a-4050-a28d-4861a65d948f'
        ]);
        $I->seeResponseContainsJson([
            'name' => 'Sra. Norma Galvão de Souza',
            'email' => 'normagalvao@gmail.com',
            'document' => '882.708.630-76',
            'city' => 'Barbacena',
            'state' => 'Minas Gerais',
            'start_date' => '2020-01-10',
            'manager_id' => '41b6c1f7-1d2a-4050-a28d-4861a65d948f'
        ]);
        $I->seeResponseContainsJson([
            'name' => 'Prof. Flavio Araújo',
            'email' => 'flavioaraujo@usp.br',
            'document' => '324.928.428-94',
            'city' => 'Piracicaba',
            'state' => 'São Paulo',
            'start_date' => '2020-06-15',
            'manager_id' => '41b6c1f7-1d2a-4050-a28d-4861a65d948f'
        ]);
    }
}
