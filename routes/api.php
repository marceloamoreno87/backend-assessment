<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(["namespace" => "Api"], function () {
    Route::post("/auth/login", "AuthController@login")->name("login");

    Route::group(["middleware" => "apiJwt"], function () {
        Route::get("/obter-colaboradores", "UserController@obterColaboradores")->name("obter_colaboradores");
        Route::delete("/excluir-colaborador/{cpf}", "UserController@excluirColaborador")->name("excluir_colaborador");
        Route::put("/importar-colaborador/{csv}", "UserController@importarColaborador")->name("importar_colaborador");
    });
});




