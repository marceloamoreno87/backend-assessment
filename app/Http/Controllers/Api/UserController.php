<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Controller;
use App\Mail\SuccessImport;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController
 * @package App\Http\Controllers\Api
 */
class UserController extends Controller
{
    /**
     * @var User
     */

    private $userService;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Employee
     */
    private $employee;

    /**
     * UserController constructor.
     * @param User $user
     */
    public function __construct(User $user, Employee $employee)
    {
        $this->user = $user;
        $this->employee = $employee;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function obterColaboradores()
    {
        $employees = $this->employee->all();
        if (!$employees) {
            return response()->json([
                "status" => false
            ])->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            "status" => true,
            "user" => $employees
        ])->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function excluirColaborador($cpf)
    {
        $user = $this->employee->where("document", $cpf)->first();
        if (!$user) {
            return response()->json([
                "status" => false,
                "message" => "Delete not complete."
            ])->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $user->delete();
        return response()->json([
            "status" => true,
            "message" => "Delete is finish."
        ])->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @param $csv
     * @return \Illuminate\Http\JsonResponse
     */
    public function importarColaborador($csv)
    {
        $userLogged = $this->me()->getData();
        $userLogged = $this->user->where("email", $userLogged->email)->first();

        $csv = $csv . ".csv";
        $file = base_path("tests\\_data\\" . $csv);
        $imports = $this->csvToArray($file);
        if (!$imports) {
            return response()->json([
                "status" => false,
                "message" => "File not found."
            ]);
        }
        
        foreach ($imports as $key => $import) {
            $employee = $this->employee->where("email", $import["email"])->first();
            if ($employee) {
                $employee->update([
                    "name" => $import["name"],
                    "email" => $import["email"],
                    "document" => $import["document"],
                    "city" => $import["city"],
                    "state" => $import["state"],
                    "start_date" => $import["start_date"]
                ]);
            } else {
                $userLogged->employees()->create([
                    "name" => $import["name"],
                    "email" => $import["email"],
                    "document" => $import["document"],
                    "city" => $import["city"],
                    "state" => $import["state"],
                    "start_date" => $import["start_date"]
                ]);
            }
        }
        $mail = New SuccessImport($userLogged);
        Mail::send($mail);
        return response()->json(["status" => true,
            "message" => "Import is finish."
        ]);
    }
}
