<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class SuccessImport
 * @package App\Mail
 */
class SuccessImport extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var
     */
    private $userLogged;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userLogged)
    {
        $this->userLogged = $userLogged;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject("Success Import");
//        $this->to("testeconvenia.noreply@gmail.com");
        $this->to($this->userLogged->email);
        return $this->view('mails.employee.batch.successful');
    }
}
