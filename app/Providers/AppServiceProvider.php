<?php

namespace App\Providers;

use App\Models\Employee;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Ramsey\Uuid\Uuid as RamseyUuid;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Employee::creating(function ($model) {
            $model->id = RamseyUuid::uuid4()->toString();
        });
    }
}
